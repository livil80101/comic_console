<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Comic
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property int $author_id
 * @property string $cover
 * @property int $enable
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Database\Factories\ComicFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|Comic newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Comic newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Comic query()
 * @method static \Illuminate\Database\Eloquent\Builder|Comic whereAuthorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Comic whereCover($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Comic whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Comic whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Comic whereEnable($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Comic whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Comic whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Comic whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Tag[] $tags
 * @property-read int|null $tags_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Message[] $messages
 * @property-read int|null $messages_count
 * @property int $is_r18 是否是成人漫畫
 * @property-read \App\Models\Author|null $author
 * @method static \Illuminate\Database\Eloquent\Builder|Comic whereIsR18($value)
 */
class Comic extends Model
{
    use HasFactory;

    protected $table = 'comic';

    public function author()
    {
        return $this->hasOne(Author::class, 'id', 'author_id');
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class, 'comic_tag');
    }

    public function messages()
    {
        return $this->hasMany(Message::class);
    }
}
