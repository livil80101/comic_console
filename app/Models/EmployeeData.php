<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\EmployeeData
 *
 * @property int $id 跟員工id同樣
 * @property string $name
 * @property int $enable 是否開通
 * @property string|null $avatar 頭像
 * @property string|null $pid 身分證字號
 * @property string|null $eid 員工識別代號
 * @property int|null $sex 性別
 * @property string|null $birthday 生日
 * @property int|null $blood 血型
 * @property string|null $tel 聯絡電話
 * @property string $address 地址
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Database\Factories\EmployeeDataFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|EmployeeData newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|EmployeeData newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|EmployeeData query()
 * @method static \Illuminate\Database\Eloquent\Builder|EmployeeData whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmployeeData whereAvatar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmployeeData whereBirthday($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmployeeData whereBlood($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmployeeData whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmployeeData whereEid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmployeeData whereEnable($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmployeeData whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmployeeData whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmployeeData wherePid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmployeeData whereSex($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmployeeData whereTel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmployeeData whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class EmployeeData extends Model
{
    use HasFactory;

    protected $table = 'employee_data';
}
