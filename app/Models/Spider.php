<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Spider
 *
 * @property int $id
 * @property int $chapter_id
 * @property string $url 抓取對象
 * @property string $url_root 爬蟲隸屬的url
 * @property string $html 暫存html
 * @property int $type_id 爬蟲對象
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\ComicChapter|null $chapter
 * @method static \Illuminate\Database\Eloquent\Builder|Spider newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Spider newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Spider query()
 * @method static \Illuminate\Database\Eloquent\Builder|Spider whereChapterId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Spider whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Spider whereHtml($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Spider whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Spider whereTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Spider whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Spider whereUrl($value)
 * @mixin \Eloquent
 */
class Spider extends Model
{
    use HasFactory;

    protected $table = 'spiders';

    //出處，目前只有一個
    const EXHENTAI = 1;

    public function chapter()
    {
        return $this->belongsTo(\App\Models\ComicChapter::class, 'chapter_id', 'id');
    }
}
