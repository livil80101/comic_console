<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Employees
 *
 * @property int $id
 * @property string $account 帳號、唯一識別
 * @property string $email 登入郵件
 * @property string|null $email_verified_at 郵件驗證時間
 * @property string $password
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Employees newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Employees newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Employees query()
 * @method static \Illuminate\Database\Eloquent\Builder|Employees whereAccount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Employees whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Employees whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Employees whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Employees whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Employees wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Employees whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Employees whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Employees extends Model
{
    use HasFactory;

    protected $table = 'employees';

    protected $hidden = [
        'password'
    ];
}
