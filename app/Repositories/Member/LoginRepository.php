<?php

namespace App\Repositories\Member;

use App\Models\Member;
use Illuminate\Support\Facades\Hash;

class LoginRepository
{
    /**
     * 會員登入
     */
    public function loginByAccount($email, $password)
    {
        $member = Member::where('email', $email)
            ->first();
        if (Hash::check($password, $member->password))
            return $member;
        return null;
    }
}
