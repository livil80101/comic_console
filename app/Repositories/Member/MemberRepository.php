<?php

namespace App\Repositories\Member;

use App\Models\Comment;
use App\Models\Member;
use App\Models\Message;

class MemberRepository
{
    public function searchMember($count, array $data)
    {
        $member = Member::query();
        $name   = $data['name'] ?? null;
        if ($name) {
            $member->where('name', 'like', "%{$name}%");
        }
        return $member
            ->withCount([
                'comments',
                'messages',
            ])
            ->orderBy('updated_at', 'desc')
            ->orderBy('id')
            ->paginate($count);
    }

    public function getMessageById($id, $count)
    {
        $message = Message::query();
        return $message->where('member_id', $id)
            ->orderBy('updated_at', 'desc')
            ->orderBy('id')
            ->paginate($count);
    }

    public function getCommentById($id, $count)
    {
        $comment = Comment::query();
        return $comment->where('member_id', $id)
            ->orderBy('updated_at', 'desc')
            ->orderBy('id')
            ->paginate($count);
    }

    public function getMemberData($id)
    {
        return Member::find($id);
    }
}
