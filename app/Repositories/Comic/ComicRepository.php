<?php

namespace App\Repositories\Comic;

use App\Http\Requests\apiGetComicRequest;
use App\Models\Comic;
use App\Models\ComicChapter;
use App\Models\ComicPage;
use App\Models\Comment;
use App\Models\Message;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ComicRepository
{
    // 漫畫列表
    public function getComicList(apiGetComicRequest $request)
    {
        $count            = $request->input('count');
        $search_id        = $request->input('id');
        $search_like_name = $request->input('like_name');


        if (!in_array($count, [10, 15, 30, 50, 100])) {
            $count = 30;
        }

        $comic = Comic::query();

        if ($search_id) {
            $comic->where('id', $search_id);
        }
        if ($search_like_name) {
            $comic->where('title', 'like', "%{$search_like_name}%");
        }

        return $comic
            ->with([
                'tags',
                'author'
            ])
            ->withCount([
                'messages'
            ])
            ->orderBy('updated_at', 'desc')
            ->orderBy('id')
            ->paginate($count);
    }

    // 單筆漫畫資料
    public function getComicById($id)
    {
        $comic = Comic::query();
        return $comic
            ->where('id', $id)
            ->with([
                'tags',
                'author'
            ])
            ->withCount([
                'messages'
            ])
            ->first();
    }

    // 取得章節
    public function getChapter($id)
    {
        return ComicChapter::find($id);
    }

    public function getComicByTag($id, $count)
    {
        $comic = Comic::query();
        return $comic->join('comic_tag', 'comic_tag.comic_id', '=', 'comic.id')
            ->where('comic_tag.tag_id', $id)
            ->paginate($count);
    }

    // 新增漫畫
    public function createComic($title, $description, $author_id, UploadedFile $cover = null)
    {
        $comic              = new Comic();
        $comic->title       = $title;
        $comic->description = $description;
        $comic->author_id   = $author_id ?? 1;
        $comic->cover       = '/images/cover.png';
        $comic->save();
        if ($cover) {
            $new_file_name = $comic->id . '.' . $cover->extension();
            $cover->storeAs('public/cover', $new_file_name);
            $comic->cover = "/storage/cover/" . $new_file_name;
            $comic->save();
        }

        //建立資料夾
        Storage::makeDirectory('public/' . $comic->id);

        return $comic;
    }

    // 新增章節
    public function createChapter($comic_id, $title)
    {
        $comic = Comic::find($comic_id);
        $path  = $this->getOrMakeComicDirectory($comic_id);
        //建立資料夾 uuid
        $folder_name = Uuid::uuid1()->toString();
        $folder      = "{$folder_name}";
        Storage::makeDirectory("{$path}/{$folder}");

        $chapter           = new ComicChapter();
        $chapter->title    = $title;
        $chapter->comic_id = $comic_id;
        $chapter->folder   = $folder;
        $chapter->sort     = $this->getLastSort($comic) + 1;
        $chapter->save();
        return $chapter;
    }

    //編輯漫畫
    public function setComicData(
        Request $request
    )
    {
        $id          = $request->input('id');
        $title       = $request->input('title');
        $description = $request->input('description');
        $author_id   = $request->input('author_id');
        $cover       = $request->file('cover');
        $enable      = $request->input('enable');
        $is_r18      = $request->input('is_r18');


        /** @var Comic $comic */
        $comic = Comic::find($id);

        if (!$comic)
            throw new NotFoundHttpException();

        if ($title)
            $comic->title = $title;
        if ($description)
            $comic->description = $description;
        if ($author_id)
            $comic->author_id = $author_id;
        if ($cover) {
            $file_name = $id . '.' . $cover->extension();
            $cover->storeAs('public/cover', $file_name);
            $comic->cover = '/storage/cover/' . $file_name;
        }
        if ($enable)
            $comic->enable = $enable;
        if ($is_r18)
            $comic->is_r18 = $is_r18;

        $comic->save();

        return Comic::whereId($id)->with('author')->first();
    }

    //編輯章節
    public function setChapterData(
        $id,
        $title = null,
        $comic_id = null,
        $sort = null
    )
    {
        /** @var ComicChapter $chapter */
        $chapter = ComicChapter::find($id);
        if ($title)
            $chapter->title = $title;
        if ($comic_id)
            $chapter->comic_id = $comic_id;
        if ($sort)
            $chapter->sort = $sort;
        $chapter->save();
        return $chapter;
    }

    public function getPageByChapterId($id)
    {
        $pages   = ComicPage::query();
        $chapter = ComicChapter::where('id', $id)
            ->select('id', 'title', 'comic_id', 'sort')
            ->first();
        $comic   = Comic::where('id', $chapter->comic_id)->first();

        return [
            'comic'   => $comic,
            'chapter' => $chapter,
            'pages'   => $pages->where('chapter_id', $id)
                ->orderBy('sort')
                ->orderBy('id')
                ->get()
        ];

    }

    // 編輯頁
    public function setPageData($id, $chapter_id = null, $sort = null, UploadedFile $file = null)
    {
        /** @var ComicPage $page */
        $page = ComicPage::find($id);
        if ($sort)
            $page->sort = $sort;

        //如果改變從屬章節的話，就複製到對應資料夾後再刪除
        if ($chapter_id && $chapter_id != $page->chapter_id) {
            /** @var ComicChapter $chapter */
            /** @var ComicChapter $chapter_old */
            $chapter     = ComicChapter::find($chapter_id);
            $chapter_old = ComicChapter::find($page->chapter_id);

            $folder     = $chapter->folder;
            $folder_old = $chapter_old->folder;
            $new_path   = Str::replace($folder_old, $folder, $page->path);
            $new_url    = Str::replace($folder_old, $folder, $page->url);

            Storage::move($page->path, $new_path);
            $page->path = $new_path;
            $page->url  = $new_url;
        }

        if ($file) {
            $del_path = $page->path;
            $chapter  = ComicChapter::find($page->chapter_id);
            $comic_id = $chapter->comic_id;
            $path     = $chapter->folder;

            $new_file_name = Uuid::uuid1()->toString();
            $file_name     = $new_file_name . '.' . $file->extension();
            $file->storeAs("public/{$comic_id}/{$path}", $file_name);
            Storage::delete($del_path);

            $page->path = "public/{$comic_id}/{$path}/{$file_name}";
            $page->url  = "/storage/{$comic_id}/{$path}/{$file_name}";
        }
        $page->save();
        return $page;
    }

    /**
     * 設定頁順序
     * @param array $pages
     * 注意這邊要求的是全部 page id
     */
    public function setPageSort($id, array $pages)
    {
        $length = count($pages);

        //如果少於20筆，就直接用迴圈update
        //反之就要用temporary table
        //基於時間考量，先不做 temporary
//        if ($length > 20) {
        foreach ($pages as $sort => $page_id) {
            ComicPage::where('id', $page_id)->update(['sort' => $sort + 1]);
        }
//        } else {
        //建立暫存表
//        }
        return ComicPage::where('chapter_id', $id)
            ->orderBy('sort')
            ->orderBy('id')
            ->get();
    }

    // 刪除頁
    public function deletePage($id)
    {
        $pages      = ComicPage::whereIn('id', $id)->get();
        $chapter_id = null;
        foreach ($pages as $page) {

            $chapter_id = $page->chapter_id;
            /** @var ComicPage $page */
            Storage::delete($page->path);
            $page->delete();
        }

        return ComicPage::where('chapter_id', $chapter_id)
            ->orderBy('sort')
            ->orderBy('id')
            ->get();
    }

    //刪除章節
    public function deleteChapter($id)
    {
        //取得底下全部page，將路徑整理成陣列
    }

    // 上傳頁面
    public function createPageByUpload($chapter_id, $file)
    {

        $chapter = ComicChapter::find($chapter_id);

        $re = [];
        if (!($chapter && $file)) {
            return $re;
        }

        /** @var UploadedFile $file */
//        foreach ($files as $file) {
        $comic_id  = $chapter->comic_id;
        $path      = $chapter->folder;
        $page_name = Uuid::uuid1()->toString();

        $file_name = "{$page_name}.{$file->extension()}";
        $file->storeAs("public/{$comic_id}/{$path}", $file_name);

        $page             = new ComicPage();
        $page->chapter_id = $chapter->id;
        $page->path       = "public/{$comic_id}/{$path}/{$file_name}";
        $page->url        = "/storage/{$comic_id}/{$path}/{$file_name}";
        $page->sort       = $this->getLastSort($chapter) + 1;
        $page->save();

        $re[] = $page;
//        }
        return ComicPage::where('chapter_id', $chapter_id)
            ->orderBy('sort')
            ->orderBy('id')->get();
    }

    //取得排序最後的號碼
    public function getLastSort($obj)
    {
        switch (1) {
            case $obj instanceof Comic:
                $build = ComicChapter::selectRaw('max(sort) as sort')->where('comic_id', $obj->id)->groupBy('comic_id')->first();
                break;
            case $obj instanceof ComicChapter:
                $build = ComicPage::selectRaw('max(sort) as sort')->where('chapter_id', $obj->id)->groupBy('chapter_id')->first();
                break;
            default:
                return 0;
        }

        return $build->sort ?? 0;
    }

    // 取得或是生成漫畫資料夾
    public function getOrMakeComicDirectory($id)
    {
        $path = 'public/' . $id;
        if (!Storage::exists($path)) {
            Storage::makeDirectory($path);
        }
        return $path;
    }

    //取得漫畫底下的章節
    public function getChaptersByComicId($comic_id)
    {
        $chapter = ComicChapter::query();

        return $chapter->where('comic_id', $comic_id)
            ->orderBy('sort')
            ->orderBy('id')
            ->withCount(['pages'])
            ->get();
    }

    //取得漫畫底下的留言
    public function getMessageByComicId($id, $count)
    {
        $message = Message::query();

        return $message->where('comic_id', $id)
            ->orderBy('updated_at', 'desc')
            ->orderBy('id')
            ->paginate($count);
    }

    //取得漫畫底下的留言
    public function getCommentByChapterId($id, $count)
    {
        $comment = Comment::query();

        return $comment->where('chapter_id', $id)
            ->orderBy('updated_at', 'desc')
            ->orderBy('id')
            ->paginate($count);
    }
}
