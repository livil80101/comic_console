<?php

namespace App\Repositories\Comic;

use App\Models\Comic;
use App\Models\Tag;
use Carbon\Carbon;

class TagRepository
{
    public function getTags()
    {
        return Tag::all();
    }

    public function setComicTags($id, array $tags)
    {
        /** @var Comic $comic */
        $comic = Comic::find($id);
        $comic->tags()->sync($tags);
        return $comic;
    }

    public function setDefaultTags()
    {
        $add = [];
        $now = Carbon::now();
        foreach ($this->tags as $tag) {
            $add[] = [
                'name'       => $tag,
                'created_at' => $now,
                'updated_at' => $now,
            ];
        }
        Tag::insert($add);
    }

    private $tags = [
        '愛情',
        '校園',
        '古裝',
        '奇幻',
        '熱血',
        '冒險',
        '魔幻',
        '神鬼',
        '搞笑',
        '萌系',
        '科幻',
        '魔法',
        '格鬥',
        '機戰',
        '戰爭',
        '競技',
        '體育',
        '勵志',
    ];
}
