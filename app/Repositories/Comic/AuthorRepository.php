<?php

namespace App\Repositories\Comic;

use App\Models\Author;
use App\Models\Comic;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpKernel\Exception\HttpException;

class AuthorRepository
{
    // 作者列表
    public function getAuthorList(Request $request)
    {
        $count            = $request->input('count');
        $search_id        = $request->input('id');
        $search_like_name = $request->input('like_name');

        if (!in_array($count, [10, 30, 50, 100])) {
            $count = 30;
        }
        $author = Author::query();

        if ($search_id) {
            $author->where('id', $search_id);
        }
        if ($search_like_name) {
            $author->where('name', 'like', "%{$search_like_name}%");
        }

        return $author->orderBy('updated_at', 'desc')
            ->with('comics')
            ->orderBy('id')
            ->paginate($count);
    }

    // 取得單作者
    public function getAuthor($id)
    {
        return Author::find($id);
    }

    //取得作者的作品
    public function getAsset($id)
    {
        //除了佚名之外，應該就只有少少幾個能破千作品
        return Comic::where('author_id', $id)
            ->orderBy('created_at', 'desc')
            ->select('id', 'title', 'cover')
            ->get();
    }

    // 編輯作者
    public function setAuthorData($id, $name = null, $description = null, UploadedFile $photo = null)
    {
        /** @var Author $author */
        $author = Author::find($id);
        if ($name)
            $author->name = $name;
        if ($description)
            $author->description = $description;
        if ($photo) {
            $path_old = $author->photo;
            Storage::delete(storage_path($path_old));

            $new_file_name = $author->id . '.' . $photo->extension();
            $photo->storeAs('/public/author', $new_file_name);//會蓋過同名的檔案，注意 jpg不會蓋過png
            $author->photo = "/storage/author/" . $new_file_name;
        }
        $author->save();
        $author->photo = $author->photo . '?t=' . time();
        return $author;
    }

    //刪除作者
    public function deleteAuthor($id)
    {
        // 刪除作者前，先取得他的漫畫，全部轉到無名氏作者後再刪除
        Comic::where('author_id', $id)->update(['author_id' => 1]);
        /** @var Author $author */
        $author = Author::find($id);

        $photo = $author->photo;
        Storage::delete(storage_path($photo));
        return $author->delete();
    }

    public function createAuthor($name, $desc, UploadedFile $photo = null)
    {
        $author              = new Author();
        $author->name        = $name;
        $author->description = $desc;
        $author->photo       = '/images/user.png';
        $author->save();
        if ($photo) {
            $new_file_name = $author->id . '.' . $photo->extension();
            $photo->storeAs('public/author', $new_file_name);
            $author->photo = "/storage/author/" . $new_file_name;
            $author->save();
        }

        return $author;
    }
}
