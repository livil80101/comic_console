<?php

namespace App\Repositories\Comic;

use App\Models\Comic;
use App\Models\Spider;
use App\Models\SpiderPreView;
use GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Psr7\Utils;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Ramsey\Uuid\Uuid;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


class SpiderRepository
{
    public function checkDir($spider_id)
    {
        //檢查是否有資料夾存在，沒有的話就建立一個
        $dir_path = storage_path("app/public/spider/{$spider_id}");
        if (!(
            File::exists($dir_path) && File::isDirectory($dir_path)
        )) {
            mkdir($dir_path, 0775, true);
        }
    }

    private $default_url = 'https://exhentai.org/g/2228088/91ca8eaa76/';

    public function getInfo(Request $request)
    {
        $url = $request->input("url", $this->default_url);

        //嘗試取得，否則建立新的spider
        $this->createGuzzle();
        $queryString = parse_url($url, PHP_URL_QUERY);
        //參數
        $params = [];
        if ($queryString) {
            parse_str($queryString, $params);
        }


        //如果先前資料庫已經有則取出來直接使用，避免再次佔用網路資源
        //不過在那之前，先拆解URL
        list($url) = explode('?', $url);

        $last_char = substr($url, -1);
        if ($last_char == '/') {
            $url = substr($url, 0, strlen($url) - 1);
        }

        //清除多餘的 params 只留下有 p 的
        $p     = $params['p'] ?? 0;
        $o_url = $url . '?p=' . $p;

        $spider = Spider::where("url", $o_url)->with("chapter")->first();

        if ($spider) {
            $body = $spider->html;
        } else {

            try {
                $res = $this->client->request('GET', $o_url);
            } catch (\GuzzleHttp\Exception\GuzzleException $exception) {
                return response()->json([
                    'message' => "輸入有問題",
                    "input"   => $request->all()
                ], 400);
            }

            $body = (string)$res->getBody();
            //清除評論區避免多餘、不必要的資料
            $body = $this->clearExHentaiComment($body);
        }
        //分解body
        $crawler = new Crawler($body);
        //取得封面
        $cover_src   = $crawler->filter('#gd1>div')->first();
        $cover_style = $cover_src->attr("style");
        $cover       = $this->getUrlByStyle($cover_style);

        //取得標題
        $title_en = $crawler->filter("#gn")->first()->text();
        $title    = $crawler->filter("#gj")->first()->text();//雖說是原文，但j可能就是日文的樣子，台灣本也是一樣

        //取得第一頁縮圖
        $gdtm_list = $crawler->filter("#gdt .gdtm>div")
            ->each(function (Crawler $node, $i) {

                return [
                    'style' => $node->attr("style"),
                    'href'  => $node->filter('a')->attr('href'),
                ];
            });

        //計算總共幾頁
        $last_page_number = $crawler->filter(".ptb td:nth-last-child(2) a")->first()->text();

        //模擬每一頁要訪問的網址
        $page_list = [];
        for ($i = 0; $i < $last_page_number; $i++) {
            $page_list[] = "{$url}?p={$i}";
        }
        if (!$spider) {
            //建立新的spider 記錄
            $spider             = new Spider();
            $spider->url        = $o_url;
            $spider->url_root   = $url;
            $spider->html       = $body;
            $spider->type_id    = Spider::EXHENTAI;
            $spider->chapter_id = null;
            $spider->save();
        }

        $cover_url = "/img/ex_hentai?spider_id={$spider->id}&url=" . $cover;
        $img_url   = "/img/ex_hentai?spider_id={$spider->id}&url=";
        foreach ($gdtm_list as $i => $item) {
            $gdtm_list[$i]['style'] = $this->insertUrl2Style(
                $item['style'], $img_url
            );
        }

        return [
            'spider_id'    => $spider->id,
            'cover'        => $cover_url,
            'title'        => $title,
            'title_en'     => $title_en,
            'gdtm_list'    => $gdtm_list,
            'page_number'  => $last_page_number,
            'page_list'    => $page_list,
            'current_page' => $p,
            'o_url'        => $o_url,
            'url'          => $url,
        ];
    }

    private $file_type;

    public function getFileType()
    {
        return $this->file_type;
    }

    //存圖片
    public function savePreviewImage($spider_id, $url)
    {
        $spv = SpiderPreView::where('url', $url)->first();
        //取得圖片

        if ($spv) {
            $storage_path    = $spv->storage_path;
            $this->file_type = $spv->content_type;
            return file_get_contents($storage_path);
        }
        $this->createGuzzle();
        //取得圖片
        $img = $this->client->request('GET', $url, ['sink' => "app/public/spider"]);
        //取得檔案類型
        $content_type    = $img->getHeader("Content-Type")[0];
        $this->file_type = $type = @explode("/", $content_type)[1];
        //取得圖片內容
        $body = $img->getBody()->getContents();

        $this->checkDir($spider_id);

        //流水號
        $uuid = Uuid::uuid1()->toString();
        $uuid = str_replace("-", "", $uuid);

        $dir_path = storage_path("app/public/spider/{$spider_id}");

        //生成url與寫入路徑
        $storage_path = storage_path("app/public/spider/{$spider_id}/{$uuid}.{$type}");
        $storage_url  = url("storage/spider/{$uuid}.{$type}");

        //寫入
        $file = fopen($storage_path, "w");
        fwrite($file, $body);
        fclose($file);

        //記錄
        //寫入資料庫
        $spv               = new SpiderPreView();
        $spv->url          = $url;
        $spv->storage_path = $storage_path;
        $spv->storage_url  = $storage_url;
        $spv->content_type = $type;
        $spv->save();


        return $body;
    }

    //取得單頁的圖片
    public function getPage($spider_id, $url)
    {
        $spv = SpiderPreView::where('url', $url)->first();

        if ($spv) {
            $storage_path    = $spv->storage_path;
            $this->file_type = $spv->content_type;
            return file_get_contents($storage_path);
        }

        $this->createGuzzle();

        try {
            $res = $this->client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $exception) {
            throw new NotFoundHttpException('輸入有問題');
        }

        $body = (string)$res->getBody();

        //分解
        $crawler = new Crawler($body);
        //取得圖片並儲存於本地
        $img_url = $crawler->filter("#img")->attr("src");

        $uuid = Uuid::uuid1()->toString();
        $uuid = str_replace("-", "", $uuid);

        //取得圖片
        $img = $this->client->request('GET', $img_url, ['sink' => "app/public/spider"]);
        //取得副檔名
        $content_type = $img->getHeader("Content-Type")[0];
        $type         = @explode("/", $content_type)[1];
        //取得圖片內容
        $body = $img->getBody()->getContents();

        //生成url與寫入路徑
        $storage_path = storage_path("app/public/spider/{$spider_id}/{$uuid}.{$type}");
        $storage_url  = url("storage/spider/{$uuid}.{$type}");

        //檢查是否有資料夾存在，沒有的話就建立一個
        $this->checkDir($spider_id);
        //寫入
        $file = fopen($storage_path, "w");
        fwrite($file, $body);
        fclose($file);

        //記錄
        //寫入資料庫
        $spv               = new SpiderPreView();
        $spv->url          = $url;
        $spv->storage_path = $storage_path;
        $spv->storage_url  = $storage_url;
        $spv->content_type = $type;
        $spv->save();


        return $body;
    }

    /** @var Client $client */
    private $client;

    //建立連結
    private function createGuzzle()
    {
        if (!$this->client) {
            $jar = CookieJar::fromArray(//<-------試試cookie
                [
                    "igneous"       => config('exhentai.igneous'),
                    "ipb_member_id" => config('exhentai.ipb_member_id'),
                    "ipb_pass_hash" => config('exhentai.ipb_pass_hash'),
                    "sk"            => config('exhentai.sk'),
                    "sl"            => config('exhentai.sl'),
                ],
                'exhentai.org'
            );

            $this->client = new Client([
                'base_uri' => 'https://exhentai.org/',
                'cookies'  => $jar,
                'stream'   => true,
            ]);

        }
    }

    //清除 ex 網的評論區
    private function clearExHentaiComment($body)
    {
        $_body = explode('<div id="cdiv" class="gm">', $body);
        $head  = $_body[0];
        $_body = explode('<script type="text/javascript" src="https://exhentai.org/z/0348/ehg_gallery.c.js"></script>', $_body[1]);

        $foot = $_body[1] ?? $_body[0];
        return $head . $foot;
    }

    private function insertUrl2Style($style, $url = '/cover/ex_hentai?url=')
    {
        $styles = @explode("url(", $style);
        return $styles[0] . 'url(' . $url . $styles[1];
    }

    //取得div的 style 背景的資訊
    private function getUrlByStyle($style)
    {
        //去頭去尾去空白
        $style = @explode("url(", $style)[1];
        $style = @explode(")", $style)[0];

        return trim($style);
    }

}
