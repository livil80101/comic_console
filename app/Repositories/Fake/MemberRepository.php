<?php

namespace App\Repositories\Fake;

use App\Models\Comment;
use App\Models\Member;
use App\Models\Message;

class MemberRepository
{
    // 建立幾個假會員
    public function createDefaultMember()
    {
        Member::factory()->count(20)->create();
    }

    // 建立假留言
    public function createDefaultMessage()
    {
        Comment::factory()->count(30)->create();
        Message::factory()->count(30)->create();
    }
}
