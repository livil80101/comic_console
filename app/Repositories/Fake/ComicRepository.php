<?php

namespace App\Repositories\Fake;

use App\Models\Author;
use App\Models\Comic;
use App\Models\ComicChapter;
use App\Models\ComicPage;
use Illuminate\Support\Facades\DB;

class ComicRepository
{
    public function createDefaultComic()
    {
        $comics = Comic::factory()
            ->count(30)
            ->create();

        foreach ($comics as $comic) {
            $c_id = $comic->id;

            $cc_array = ComicChapter::factory()
                ->count(10)
                ->setComicId($c_id)
                ->create();

            foreach ($cc_array as $cc) {
                $cc_id = $cc->id;

                ComicPage::factory()
                    ->count(20)
                    ->setChapterId($cc_id)
                    ->create();
            }
        }
    }

    public function createComicTag()
    {
        DB::table('comic_tag')->insert([
            ['tag_id' => 1, 'comic_id' => 1],
            ['tag_id' => 2, 'comic_id' => 1],
            ['tag_id' => 3, 'comic_id' => 1],
            ['tag_id' => 4, 'comic_id' => 1],
            ['tag_id' => 5, 'comic_id' => 1],
            ['tag_id' => 1, 'comic_id' => 2],
            ['tag_id' => 1, 'comic_id' => 3],
            ['tag_id' => 1, 'comic_id' => 4],
            ['tag_id' => 1, 'comic_id' => 5],
        ]);
    }

    public function createAuthor()
    {
        Author::factory()->count(5)->create();
    }
}
