<?php
/**
 * 判定是否是給外部的 api
 *
 * @return boolean
 */
function is_member_api()
{
    $prefix = request()->route()->getPrefix();
    //回傳 api/v1 或是 api/v2 之類的都是 true
    return strpos($prefix, 'pi/v') > 0;
}
