<?php

namespace App\Http\Controllers\Comic;

use App\Http\Controllers\Controller;
use App\Repositories\Comic\SpiderRepository;
use GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

//這個 controller 就歸 ex hentai所用了
class SpiderController extends Controller
{
    private $default_url_cover = 'https://exhentai.org/t/3c/c4/3cc48595f5d99b559cae23813899341bf1b22183-2047035-2120-3020-jpg_250.jpg';
    private $default_page = 'https://exhentai.org/s/3cc48595f5/2228088-1';

    public function exHentai(Request $request)
    {
//        $url = $request->input("url", $this->default_url);
        $re = $this->SpiderRepository->getInfo($request);
        return response()->json($re, 200, [], JSON_UNESCAPED_SLASHES);
    }

    public function showExHentaiImage(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'spider_id' => 'required|exists:spiders,id',
            'url'       => 'required',
        ]);
        if ($validator->fails()) {
            throw new NotFoundHttpException();
        }

        $spider_id = $request->input('spider_id');
        $url       = $request->input('url');
        $body      = $this->SpiderRepository->savePreviewImage($spider_id, $url);

        $response = Response::make($body, 200);
        $response->header("Content-Type", $this->SpiderRepository->getFileType());

        return $response;
    }

    //透過網址預覽圖片
    public function showExHentaiCover(Request $request)
    {
        $image = $request->input("url", $this->default_url_cover);

        //建立cookie
        $jar      = CookieJar::fromArray(
            [
                "igneous"       => config('exhentai.igneous'),
                "ipb_member_id" => config('exhentai.ipb_member_id'),
                "ipb_pass_hash" => config('exhentai.ipb_pass_hash'),
                "sk"            => config('exhentai.sk'),
                "sl"            => config('exhentai.sl'),
            ],
            'exhentai.org'
        );
        $client   = new Client([
            'base_uri' => 'https://exhentai.org/',
            'cookies'  => $jar,
            'stream'   => true,
        ]);
        $path     = storage_path("app/public/comic/a.jpg");
        $res      = $client->request('GET', $image, ['sink' => $path]);
        $body     = $res->getBody()->getContents();
        $response = Response::make($body, 200);
        $response->header("Content-Type", 'image/jpeg');

        return $response;
    }

    public function getPageImage(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'spider_id' => 'required|exists:spiders,id',
            'url'       => 'required',
        ]);
        if ($validator->fails()) {
            throw new NotFoundHttpException();
        }
        $spider_id = $request->input('spider_id');
        $url       = $request->input('url' , $this->default_page);

        $body      = $this->SpiderRepository->getPage($spider_id, $url);

        $response = Response::make($body, 200);
        $response->header("Content-Type", 'image/jpeg');

        return $response;
    }

    private $SpiderRepository;

    public function __construct(SpiderRepository $SpiderRepository)
    {
        $this->SpiderRepository = $SpiderRepository;

    }
}
