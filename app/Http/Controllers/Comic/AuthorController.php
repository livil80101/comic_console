<?php

namespace App\Http\Controllers\Comic;

use App\Http\Controllers\Controller;
use App\Http\Requests\Comic\apiAddAuthorRequest;
use App\Models\Author;
use App\Repositories\Comic\AuthorRepository;
use Illuminate\Http\Request;

class AuthorController extends Controller
{
    public function __construct(AuthorRepository $authorRepository)
    {
        $this->authorRepository = $authorRepository;
    }

    public function getAuthorList(Request $request)
    {
        return $this->authorRepository->getAuthorList($request);
    }

    public function getAuthor($id)
    {
        return $this->authorRepository->getAuthor($id);
    }

    public function getAuthorAsset($id)
    {
        return response()->json([
            'comic' => $this->authorRepository->getAsset($id),
        ]);
    }

    public function addAuthor(apiAddAuthorRequest $request)
    {
        return response()->json(
            $this->authorRepository->createAuthor(
                $request->input('name'),
                $request->input('description'),
                $request->file('photo')
            )
        );
    }

    public function editAuthor(Request $request, $id)
    {
        if ($id == 1) {
            return response()->json([
                'message' => "佚名不能修改！"
            ], 400);
        }

        return response()->json(
            $this->authorRepository->setAuthorData(
                $id,
                $request->input('name'),
                $request->input('description'),
                $request->file('photo')
            )
        );
    }

    public function removeAuthor($id)
    {
        $this->authorRepository->deleteAuthor($id);
        return response()->json([]);
    }

    private $authorRepository;
}
