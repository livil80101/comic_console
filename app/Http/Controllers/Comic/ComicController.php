<?php

namespace App\Http\Controllers\Comic;

use App\Http\Controllers\Controller;
use App\Http\Requests\apiAddComicRequest;
use App\Http\Requests\apiGetComicRequest;


use App\Http\Requests\Comic\apiAddChapterRequest;
use App\Http\Requests\Comic\apiAddPageRequest;
use App\Http\Requests\Comic\apiCommentMessageRequest;
use App\Http\Requests\Comic\apiEditChapterRequest;
use App\Http\Requests\Comic\apiEditComicRequest;
use App\Http\Requests\Comic\apiEditPageRequest;
use App\Http\Requests\Comic\apiSetComicTagsRequest;
use App\Repositories\Comic\ComicRepository;
use App\Repositories\Comic\SpiderRepository;
use App\Repositories\Comic\TagRepository;
use Illuminate\Http\Request;

class ComicController extends Controller
{
    public function __construct(
        ComicRepository  $comicRepository,
        TagRepository    $tagRepository
    )
    {
        $this->comicRepository  = $comicRepository;
        $this->tagRepository    = $tagRepository;
    }

    //取得漫畫列表
    public function getComic(apiGetComicRequest $request)
    {
        return response()->json(
            $this->comicRepository->getComicList($request)
        );
    }

    // 新增漫畫
    public function addComic(apiAddComicRequest $request)
    {
        return $this->comicRepository->createComic(
            $request->input('title'),
            $request->input('description', '無備註'),
            $request->input('author_id', 0),
            $request->file('cover')
        );
    }

    //漫畫資料
    public function getComicData($id)
    {
        return response()->json(
            $this->comicRepository->getComicById($id)
        );
    }

    //漫畫留言
    public function getMessage(apiCommentMessageRequest $request, $id)
    {
        return response()->json(
            $this->comicRepository->getMessageByComicId(
                $id, $request->input('count')
            )
        );
    }

    //章節留言
    public function getComment(apiCommentMessageRequest $request, $id)
    {
        return response()->json(
            $this->comicRepository->getCommentByChapterId(
                $id, $request->input('count')
            )
        );
    }

    // 取得章節
    public function getChapters($id)
    {
        return $this->comicRepository->getChaptersByComicId($id);
    }

    // 新增章節
    public function addChapter(apiAddChapterRequest $request)
    {
        $chapter = $this->comicRepository->createChapter($request->input('comic_id'), $request->input('title'));
        $pages   = $this->comicRepository->createPageByUpload($chapter, $request->file('file'));

        return $this->comicRepository->getChaptersByComicId($request->input('comic_id'));
    }

    // 編輯漫畫
    public function editComic(apiEditComicRequest $request)
    {
        return response()->json(
            $this->comicRepository->setComicData(
                $request
            )
        );
    }

    // 編輯章節
    public function editChapter(apiEditChapterRequest $request)
    {
        return response()->json(
            $this->comicRepository->setChapterData(
                $request->input('id'),
                $request->input('title'),
                $request->input('comic_id'),
                $request->input('sort'),
            )
        );
    }

    //取得頁數資料
    public function getPages($id)
    {
        return response()->json(
            $this->comicRepository->getPageByChapterId($id) ?? []
        );
    }

    // 編輯頁
    public function editPage(apiEditPageRequest $request)
    {
        return response()->json(
            $this->comicRepository->setPageData(
                $request->input('id'),
                $request->input('chapter_id'),
                $request->input('sort'),
                $request->file('file')
            )
        );
    }

    // 刪除頁
    public function deletePage(Request $request)
    {
        $request->validate([
            'id' => 'array|required',

        ]);

        return response()->json([
            'pages' => $this->comicRepository->deletePage($request->input('id'))
        ]);
    }

    //編輯頁順序
    public function setPageSort(Request $request)
    {
        $request->validate([
            'id'    => 'required|exists:comic_chapters,id',//章節id
            'pages' => 'array',
        ]);

        return response()->json([
            'pages' => $this->comicRepository->setPageSort(
                $request->input('id'),
                $request->input('pages', [])
            )
        ]);
    }

    // 上傳頁
    public function addPage(apiAddPageRequest $request)
    {
        return response()->json([
            'pages' => $this->comicRepository->createPageByUpload(
                $request->input('chapter_id'),
                $request->file('file')
            )
        ]);

    }

    // 刪除章節
    public function deleteChapter(Request $request)
    {
        $request->validate(['id' => 'required|exists:comic_chapters,id']);

    }

    // 刪除 不在issues
    public function freezeComic()
    {

    }

    //設定標籤
    public function setComicTags(apiSetComicTagsRequest $request, $id)
    {
        return response()->json(
            $this->tagRepository->setComicTags($id, $request->input('tags'))
        );
    }


    private $comicRepository;
    private $tagRepository;
}
