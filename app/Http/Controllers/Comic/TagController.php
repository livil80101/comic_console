<?php

namespace App\Http\Controllers\Comic;

use App\Http\Controllers\Controller;
use App\Repositories\Comic\ComicRepository;
use App\Repositories\Comic\TagRepository;
use Illuminate\Http\Request;

class TagController extends Controller
{
    public function __construct(TagRepository $tagRepository, ComicRepository $comicRepository)
    {
        $this->tagRepository   = $tagRepository;
        $this->comicRepository = $comicRepository;
    }

    public function tags()
    {
        return response()->json(
            $this->tagRepository->getTags()
        );
    }

    public function comic(Request $request, $id)
    {
        $request->validate(["count" => "nullable|in:10,30,50,100",]);

        return response()->json(
            $this->comicRepository->getComicByTag(
                $id,
                $request->input('count')
            )
        );
    }

    private $tagRepository;
    private $comicRepository;
}
