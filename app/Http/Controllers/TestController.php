<?php

namespace App\Http\Controllers;

use App\Http\Requests\Comic\apiEditComicRequest;
use App\Models\Member;
use App\Models\User;
use App\Repositories\Comic\ComicRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class TestController extends Controller
{
    public function test()
    {
        $this->createUser();
        return view("test");
    }

    public function createUser()
    {
//        $user           = new User();
//        $user->email    = "AAAA@gmail.com";
//        $user->name     = "AAAA";
//        $user->password = Hash::make("AAAA");
//        $user->save();

//        Member::where('id', '>=', 1)
//            ->update(['password' => Hash::make('1111')]);
    }

    public function post(apiEditComicRequest $request)
    {
        /** @var ComicRepository $comicRepository */
        $comicRepository = resolve(ComicRepository::class);

        $a = $comicRepository->setComicData(
            $request->input('id'),
            $request->input('title'),
            $request->input('description'),
            $request->input('author_id'),
            $request->file('cover'),
            $request->input('enable')
        );
        dd($a);
//        $path = 'public/1/4f02f4aa-9815-11ec-b51c-0242ac130007/4f045dc2-9815-11ec-ba81-0242ac130007.png';
//        Storage::delete($path);
    }
}
