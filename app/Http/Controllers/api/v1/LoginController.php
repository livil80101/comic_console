<?php

namespace App\Http\Controllers\api\v1;

use App\Http\Controllers\Controller;
use App\Models\Member;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function login(Request $request)
    {
        //總之先取得一個會員
        $member = Member::first();

        //
        Auth::guard('members')->login($member);

        /** @var Member $auth */
        $auth  = auth()->guard('members')->user();
        $token = $auth->createToken('Members')->accessToken;

//        dd($token);
        return response()->json([
            'token' => $token,
            'user'  => $auth,
        ]);
    }
}
