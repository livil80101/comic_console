<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use App\Http\Requests\Member\apiGetCommentMessageRequest;
use App\Http\Requests\Member\apiSearchMemberRequest;
use App\Repositories\Member\MemberRepository;
use Illuminate\Http\Request;

class MemberController extends Controller
{
    public function __construct(MemberRepository $memberRepository)
    {
        $this->memberRepository = $memberRepository;
    }

    public function search(apiSearchMemberRequest $request)
    {
        return response()->json(
            $this->memberRepository->searchMember(
                $request->input('count', 30),
                $request->all()
            )
        );
    }

    public function getMember($id)
    {
        return response()->json(
            $this->memberRepository->getMemberData($id)
        );
    }

    public function getMessage(apiGetCommentMessageRequest $request, $id)
    {
        return response()->json(
            $this->memberRepository->getMessageById(
                $id,
                $request->input('count')
            )
        );
    }
    public function getComment(apiGetCommentMessageRequest $request, $id)
    {
        return response()->json(
            $this->memberRepository->getCommentById(
                $id,
                $request->input('count')
            )
        );
    }

    private $memberRepository;
}
