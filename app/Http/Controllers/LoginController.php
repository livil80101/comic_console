<?php

namespace App\Http\Controllers;

use App\Repositories\Member\LoginRepository;
use App\Services\LoginService;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    //
    public function apiLogin(Request $request)
    {
        $redirect_url = $request->input('redirect_url');

        return view('member.login', [
            // 登入用
            "email"    => 'kautzer.donny@yahoo.com',
            "password" => '1111',

            'redirect_url' => $redirect_url
        ]);
    }

    public function action(Request $request)
    {
        //帳號密碼
        $email = $request->input('email');
        $pw    = $request->input('password');

        //取得轉跳網址
        $redirect_url = $request->input('redirect_url');

        $this->loginService->loginByMemberAccount($email, $pw);
        //如果有轉跳的網址的話，就轉到對應的網站並且夾帶token，反之就轉到預設的會員服務中心
        if ($redirect_url) {
            $query = http_build_query(['code' => $this->loginService->getToken()]);

            return redirect()->to('http://' . $redirect_url . "?" . $query);
        }
        return redirect(route('member.service'));
    }

    public function __construct(LoginService $loginService)
    {
        $this->loginService = $loginService;
    }


    private $loginService;
}
