<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class apiGetComicRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "page"  => 'nullable|integer', // 第幾頁
            "count" => 'nullable|integer', // 筆數
        ];
    }
}
