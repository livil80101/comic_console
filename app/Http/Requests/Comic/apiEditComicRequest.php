<?php

namespace App\Http\Requests\Comic;

use Illuminate\Foundation\Http\FormRequest;

class apiEditComicRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id'          => 'required|exists:comic,id',
            "title"       => 'nullable|string',
            "description" => 'nullable|string',
            "author_id"   => 'nullable|exists:authors,id',
            "cover"       => 'nullable|file|mimes:jpeg,jpg,png',
            "enable"      => 'nullable|boolean',
        ];
    }
}
