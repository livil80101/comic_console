<?php

namespace App\Http\Requests\Comic;

use Illuminate\Foundation\Http\FormRequest;

class apiEditPageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return 1;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id'         => 'required|exists:comic_pages,id',
            'chapter_id' => 'nullable|exists:comic_chapters,id',
            'sort'       => 'nullable|integer',
            'file'       => 'nullable|file',
        ];
    }
}
