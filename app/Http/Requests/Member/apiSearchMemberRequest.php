<?php

namespace App\Http\Requests\Member;

use Illuminate\Foundation\Http\FormRequest;

class apiSearchMemberRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name"  => "nullable|string",
            "count" => "nullable|in:10,30,50,100",
            //之後補上tag搜尋

        ];
    }
}
