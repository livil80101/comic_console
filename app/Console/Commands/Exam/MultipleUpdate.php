<?php

namespace App\Console\Commands\Exam;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;

class MultipleUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = "exam:multi_update_1"
    . " {--stage=0} ";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '資料庫多筆更新範例:建立10000筆假資料';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $stage = $this->option('stage');

        switch ($stage) {
            case 1:
                printf("第一階段:\n");
                $this->setDefaultData();
                break;
            case 2:
                printf("第二階段:\n");
                $this->multipleUpdate();
                break;
            case 3:
                printf("第三階段:\n");
                $this->reset();
                break;
            case 4:
                printf("全部清空:\n");
                $this->clear();
                break;
            case 5:
                printf("使用TEMPORARY:\n");
                $this->useTemporary();
                break;
            default:
                printf($this->message);
                break;
        }
        $this->info('結束');
        return 0;
    }

    //region 階段1

    /**
     * 清除既有的資料
     */
    public function setDefaultData()
    {
        $this->clear();

        printf("建立新資料\n");
        $faker        = Faker::create();
        $check_number = 200;
        $input_buffer = [];
        for ($i = 1; $i <= 10000; $i++) {
            $input_buffer[] = [
                'id'    => $i,
                'name'  => $faker->name(30),
                'email' => $faker->email,
            ];

            if (count($input_buffer) >= $check_number) {
                DB::connection('exam')->table('multi_set')->insert($input_buffer);
                DB::connection('exam')->table('multi_default')->insert($input_buffer);

                $input_buffer = [];
            }
        }
        if (count($input_buffer) > 0) {
            DB::connection('exam')->table('multi_set')->insert($input_buffer);
            DB::connection('exam')->table('multi_default')->insert($input_buffer);

            $input_buffer = [];
        }

        for ($i = 1; $i <= 10000; $i++) {
            $name           = sprintf("%05d", $i);
            $input_buffer[] = [
                'id'    => $i * 2,
                'name'  => $name,
                'email' => $name . "@gmail.com",
            ];

            if (count($input_buffer) >= $check_number) {
                DB::connection('exam')->table('multi_input')->insert($input_buffer);

                $input_buffer = [];
            }
        }
        if (count($input_buffer) > 0) {
            DB::connection('exam')->table('multi_input')->insert($input_buffer);
        }
    }

    //endregion

    //region 階段 2
    /**
     * 將 input 的資料寫進 set 裡面
     */
    public function multipleUpdate()
    {
        //對既存的資料做 update
        $this->info("對既存的資料做 update...");
        $sql_update = <<<SQL
UPDATE
    `multi_set`
JOIN
    multi_input on multi_set.id = multi_input.id
SET
    multi_set.name = multi_input.name,
    multi_set.email = multi_input.email
SQL;
        DB::connection('exam')->statement($sql_update);

        //對不存在的資料做 insert
        $this->info("對不存在的資料做 insert...");
        $sql_insert = <<<SQL
INSERT INTO `multi_set`
SELECT * FROM `multi_input`
WHERE
NOT EXISTS (
	SELECT
    	*
    from
	    `multi_set`
    WHERE
    	multi_input.id = multi_set.id
)
SQL;
        DB::connection('exam')->statement($sql_insert);
    }

    //endregion

    //region 階段 3
    /**
     * 清空 set ，並且將 default 寫進去
     */
    public function reset()
    {
        printf("清空 multi_set 的資料\n");
        DB::connection('exam')->table('multi_set')->delete();

        //重新寫入
        $this->info("重新寫入...");
        $sql_into = <<<SQL
INSERT INTO `multi_set`
SELECT *
FROM `multi_default`
SQL;
        DB::connection('exam')->statement($sql_into);
    }

    //endregion
    //region 階段 4
    /**
     * 清空 set ，並且將 default 寫進去
     */
    public function clear()
    {
        printf("清空既有的資料\n");
        DB::connection('exam')->table('multi_set')->delete();
        DB::connection('exam')->table('multi_input')->delete();
        DB::connection('exam')->table('multi_default')->delete();
    }

    //endregion

    //region 階段 5
    /**
     * 使用鏡像的方式寫入
     */
    public function useTemporary()
    {
        $this->info('建立鏡像資料表...');
        $t_name = 'temp' . time();
        $this->info('鏡像資料表名:' . $t_name);

        $sql_create = "CREATE TEMPORARY table {$t_name} like multi_set";
        DB::connection('exam')->statement($sql_create);

        $this->info('添加資料');
        $now = Carbon::now();
        DB::connection('exam')->table($t_name)->insert([
            ['id' => 1, 'name' => 'AAAAA', 'email' => 'AAAAA', "updated_at" => $now],
            ['id' => 2, 'name' => 'BBBB', 'email' => 'BBBB', "updated_at" => $now],
            ['id' => 3, 'name' => 'CCC', 'email' => 'CCC', "updated_at" => $now],
            ['id' => 4, 'name' => 'DDDDD', 'email' => 'DDDDD', "updated_at" => $now],
            ['id' => 55555, 'name' => 'EEEE', 'email' => 'EEEE', "updated_at" => $now],
        ]);

        $this->info('多筆更新');
        $sql_update = <<<SQL
UPDATE
    `multi_set`
JOIN
    {$t_name} on multi_set.id = {$t_name}.id
SET
    multi_set.name = {$t_name}.name,
    multi_set.email = {$t_name}.email,
    multi_set.updated_at = {$t_name}.updated_at;
SQL;
        DB::connection('exam')->statement($sql_update);

        $this->info('多筆新增');
        $sql_insert = <<<SQL
INSERT INTO `multi_set`
SELECT * FROM {$t_name}
WHERE
NOT EXISTS (
	SELECT
    	*
    from
	    `multi_set`
    WHERE
    	{$t_name}.id = multi_set.id
);
SQL;
        DB::connection('exam')->statement($sql_insert);

        $this->info("刪除鏡像資料表{$t_name}");
        DB::connection('exam')->statement("DROP TABLE IF EXISTS {$t_name};");
    }

    //endregion
    private $message = <<<MSG
1 清除既有的資料
2 將 input 的資料寫進 set 裡面
3 清空 set ，並且將 default 寫進去
4 全部清空

MSG;

}
