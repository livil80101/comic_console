<?php

namespace App\Services;

use App\Models\Member;
use App\Repositories\Member\LoginRepository;
use Illuminate\Support\Facades\Auth;

/**
 * 登入相關
 */
class LoginService
{
    /**
     * 標準帳密登入
     */
    public function loginByMemberAccount($email, $password)
    {
        $member = $this->loginRepository->loginByAccount($email, $password);
        if ($member)
            Auth::guard('members')->login($member);
        else
            dd('這邊炸登入失敗');
    }

    /**
     * 使用google登入
     */
    public function loginByGoogle()
    {

    }

    public function getToken()
    {
        /** @var Member $auth */
        $auth = auth()->guard('members')->user();
        return $auth->createToken('Members')->accessToken;
    }

    public function __construct(LoginRepository $loginRepository)
    {
        $this->loginRepository = $loginRepository;
    }

    private $loginRepository;
}
