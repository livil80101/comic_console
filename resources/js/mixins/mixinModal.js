import image from "../library/image";

let mixObj = {
    data() {
        return {
            ref_name: 'modal'
        }
    },
    methods: {
        open(target) {
            if (!target)
                target = this.ref_name
            this.$refs[target].open();
        },
        hide(target) {
            if (!target)
                target = this.ref_name
            this.$refs[target].hide();
        }
    },
};

export default mixObj
