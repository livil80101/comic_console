// 對圖片常用的
import image from "../library/image";

let mixObj = {
    data() {
        return {
            accept : ".jpg,.jpeg,.png",
            enctype: "multipart/form-data",
        }
    },
    methods: {
        createPreviewURL(f) {
            return this.edit_preview_url = image.createPreviewURL(f)
        },
        removePreviewURL(url) {
            image.removePreviewURL(url)
            return null
        },
        /**
         * 檢查是否是 jpg png
         * */
        isImage(type, allow = null) {
            //只接受 jpg png 兩種
            if (!allow)
                allow = ['image/jpeg', 'image/jpg', 'image/png'];
            return allow.indexOf(type) >= 0
        },
    },
};

export default mixObj
