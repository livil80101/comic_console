import Vue from "vue";

function setLoader(el, value) {
    let {loading, type, message} = value;

    //一旦變動就刪掉舊有的
    el.querySelector('.load_bg')?.remove()
    if (!loading) {
        return
    }

    let className = 'loader'
    switch (type) {
        //上傳
        case 'upload':
            className = 'loader_upload'
            break
        //下載
        case 'download':
            className = 'loader_download'
            break
        //訊息
        case 'message':
            className = 'loader_message'
            break
        //刪除
        case 'delete':
            className = 'loader_delete'
            break

    }

    let bg       = document.createElement('div')
    bg.className = 'load_bg'

    let loader       = document.createElement('div')
    loader.className = className
    bg.append(loader)

    //如果有訊息的話，就添加上去
    if (type == 'upload' || type == 'download') {
        if (message && message.length > 0) {
            let divMessage       = document.createElement('div')
            divMessage.className = 'message'
            divMessage.innerText = message
            bg.append(divMessage)
        }
    }


    //如果是刪除中就加上訊息
    if (type == 'delete') {
        loader.innerText = 'Deleting'
    }

    el.append(bg);
}

//參考 https://cssloaders.github.io/
Vue.directive('loader', {

    update(el, {value}) {
        setLoader(el, value)
    },
    //第一次建構
    bind(el, {value}, v_node) {
        setLoader(el, value)
    },
})
