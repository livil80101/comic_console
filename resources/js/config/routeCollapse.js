//這邊記錄 sidebar 的collapse隸屬，之後每次新增都要追加
export default {
    'root' : '',
    'empty': '',

    'comic.list'  : 'comic',
    'comic.detail': 'comic',
    'comic.add'   : 'comic',
    'comic.pages' : 'comic',

    "spider.exhentai": 'comic',

    'author.list'  : 'comic',
    'author.detail': 'comic',

    'employee.list'        : 'employee',
    'employee.detail'      : 'employee',
    'employee.subscription': 'employee',

    'other.loader': 'other',

};
