export default function (title) {
    if (title) {
        document.title = title
    } else {
        document.title = '後台'
    }
}
