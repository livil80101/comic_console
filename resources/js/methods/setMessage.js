import Swal from "sweetalert2";

let Toast = Swal.mixin({
    toast            : true,
    position         : 'top-end',
    showConfirmButton: false,
    timer            : 3000,
    timerProgressBar : true,
    didOpen          : (toast) => {
        toast.addEventListener('mouseenter', Swal.stopTimer)
        toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
});

export default function (message, icon = 'success') {
    // warning
    // error
    // success
    // info
    // question
    Toast.fire({
        icon,
        title: message
    })
}
