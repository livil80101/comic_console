import Empty from "../views/other/Empty";

import Home from "../views/dashboard/Home";

import comicList from "../views/comic/List";
import comicAdd from "../views/comic/Add";
import comicDetail from "../views/comic/Detail2";
import comicPage from "../views/comic/Page";

import authorList from '../views/author/List'
import authorDetail from '../views/author/Detail'

import employeeList from "../views/employee/List";
import employeeDetail from "../views/employee/Detail";
import employeeSubscription from "../views/employee/Subscription";

import VueRouter from "vue-router";
import setDocumentTitle from "../methods/setDocumentTitle";
import ShowLoader from "../views/other/ShowLoader";
import SpiderExHentai from "../views/comic/SpiderExHentai";

const beforeEnter = (to, from, next) => {
    //設定標題
    let title = to?.meta?.title
    setDocumentTitle(title)

    next();
}
const routes      = [
    // 404
    {
        path     : '/empty',
        name     : 'empty',
        component: Empty,
        beforeEnter
    },
    // 首頁
    {
        path     : '/',
        name     : 'root',
        component: Home,
        beforeEnter
    },
    // 漫畫
    {
        path     : '/comic',
        name     : 'comic.list',
        component: comicList,
        meta     : {title: '漫畫列表'},
        beforeEnter
    },
    {
        path     : '/comic/:id',
        name     : 'comic.detail',
        component: comicDetail,
        beforeEnter
    },
    {
        path     : '/add_comic',
        name     : 'comic.add',
        component: comicAdd,
        beforeEnter
    },
    {
        path     : '/chapter/:id',
        name     : 'comic.pages',
        component: comicPage,
        beforeEnter
    },
    {
        path     : '/spider/exhentai',
        name     : 'spider.exhentai',
        component: SpiderExHentai,
        meta     : {title: '爬蟲'},
        beforeEnter
    },

    //作者
    {
        path     : '/author',
        name     : 'author.list',
        component: authorList,
        meta     : {title: '作者列表'},
        beforeEnter
    },
    {
        path     : '/author/:id',
        name     : 'author.detail',
        component: authorDetail,
        meta     : {title: '作者資料'},
        beforeEnter
    },

    // 員工
    {
        path     : '/employee',
        name     : 'employee.list',
        component: employeeList,
        beforeEnter
    },
    {
        path     : '/employee/:id',
        name     : 'employee.detail',
        component: employeeDetail,
        beforeEnter
    },
    {
        path     : '/employee/:id/subscription',
        name     : 'employee.subscription',
        component: employeeSubscription,
        beforeEnter
    },

    //其他展示區
    {
        path     : '/other/loader',
        name     : 'other.loader',
        component: ShowLoader,
        meta     : {title: '展示讀取中'},
        beforeEnter
    }
];

const router = new VueRouter({
    mode: 'history',
    base: '/',
    routes
});
export default router
