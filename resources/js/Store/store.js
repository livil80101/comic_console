import Vue from 'vue';
import Vuex from 'vuex';

import load from "./modules/load";
import comic from "./modules/comic";
import author from "./modules/author";
import spider from "./modules/spider";

const state     = {}
const getters   = {}
const actions   = {}
const mutations = {}

Vue.use(Vuex);
const store = new Vuex.Store({
    state,
    getters,
    actions,
    mutations,
    modules: {
        load,
        comic,
        author,
        spider,
    }
})
export default store;
