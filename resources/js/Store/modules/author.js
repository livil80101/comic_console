import axios from "axios";
import setMessage from "../../methods/setMessage";

const state     = {
    list         : [],
    current_page : 1, // 當前頁數
    first_page_ur: null,
    from         : null,
    last_page    : null, // 最後一頁
    last_page_url: null,
    links        : null,
    next_page_url: null,
    path         : null,
    per_page     : null,
    prev_page_url: null,
    to           : null,
    total        : null, // 總比數

    author_data: null,

    new_author_id: null,

    count: 15, // 筆數

    author_asset: [],//作者作品
    search_asset: [],//作者作品
}
const getters   = {
    list: state => state.list,

    pages    : state => state.pages,
    links    : state => state.links,
    last_page: state => state.last_page,
    total    : state => state.total,
    to       : state => state.to,
    count    : state => state.count,

    author_data : state => state.author_data,
    asset       : state => state.author_asset,
    search_asset: state => state.search_asset,
}
const actions   = {
    //取得漫畫列表
    getAuthorList({state, commit, dispatch}, page) {
        dispatch('load/setLoading', {target: 'author_list', flag: true}, {root: true})

        axios.get('/api/author', {
            params: {
                page,
                count: state.count,
            }
        }).then((resp) => {
            commit('setAuthorList', resp.data)
        }).finally(() => {
            dispatch('load/setLoading', {target: 'author_list', flag: false}, {root: true})
        })
    },
    createAuthor({commit, dispatch}, data) {
        dispatch('load/setLoading', {target: 'author_create', flag: true}, {root: true})

        axios.post(
            '/api/add_author', data
        ).then(({data}) => {
            let {id} = data;

            commit('setNewAuthorID', id)
            dispatch('getAuthorList', 1)
        }).catch(({response}) => {
            commit('setNewAuthorID', null)
        }).finally(() => {
            dispatch('load/setLoading', {target: 'author_create', flag: false}, {root: true})
        });
    },
    //取得作者資訊
    getAuthorByID({commit, dispatch}, id) {
        dispatch('load/setLoading', {target: 'author_detail', flag: true}, {root: true})

        axios.get('/api/author/' + id).then(({data}) => {
            commit('setAuthorData', data)
        }).catch(({response}) => {
            commit('setAuthorData', null)
        }).finally(() => {
            dispatch('load/setLoading', {target: 'author_detail', flag: false}, {root: true})
        })
    },
    //設定作者
    setAuthorByID({commit, dispatch}, data) {
        dispatch('load/setLoading', {target: 'author_detail_edit', flag: true}, {root: true})

        let id = data.get('id')
        axios.post('/api/author/' + id, data, {
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        }).then(resp => {
            commit('setAuthorData', resp.data)
            setMessage('修改成功')
        }).catch(({response}) => {

        }).finally(() => {
            dispatch('load/setLoading', {target: 'author_detail_edit', flag: false}, {root: true})
        })
    },
    //取得該作者的作品
    getAuthorAssetByID({commit, dispatch}, id) {
        dispatch('load/setLoading', {target: 'author_asset', flag: true}, {root: true})

        axios.get('/api/author/' + id + '/asset').then(({data}) => {
            commit('setAsset', data)
        }).catch(({response}) => {
            commit('setAsset', null)
        }).finally(() => {
            dispatch('load/setLoading', {target: 'author_asset', flag: false}, {root: true})
        })
    },
    //取得漫畫資料
    searchComic({commit, dispatch}, params) {
        dispatch('load/setLoading', {target: 'author_asset_search', flag: true}, {root: true})

        params.page  = 1
        params.count = 10
        axios.get('/api/comic', {
            params
        }).then(({data}) => {
            commit('setSearchAsset', data)
        }).finally(() => {
            dispatch('load/setLoading', {target: 'author_asset_search', flag: false}, {root: true})
        })
    },
    clearSearchComic({commit, dispatch}) {
        commit('setSearchAsset', {data: []})
    },
    //關聯指定作者與漫畫
    setAuthorAsset({commit, dispatch}, {author_id, comic_id}) {
        dispatch('load/setLoading', {target: 'author_asset_setting', flag: true}, {root: true})

        axios.post('/api/edit_comic', {
            id: comic_id,
            author_id
        }).then((resp) => {
        }).catch(({response}) => {
        }).finally(() => {
            dispatch('getAuthorAssetByID', author_id)
            dispatch('load/setLoading', {target: 'author_asset_setting', flag: false}, {root: true})
            setMessage('修改成功')
        })
    },
    //刪除作者
    deleteAuthorByID({commit, dispatch, state}) {
        dispatch('load/setLoading', {target: 'author_delete', flag: true}, {root: true})
        let id = state?.author_data?.id
        if (!id) {
            dispatch('load/setLoading', {target: 'author_delete', flag: false}, {root: true})
        }
        axios.delete('/api/author/' + id).then(({data}) => {
            setMessage('刪除成功！')
        }).finally(() => {
            dispatch('load/setLoading', {target: 'author_delete', flag: false}, {root: true})
        })


    },
}
const mutations = {
    setAuthorList(state, data) {
        state.list           = data.data
        state.current_page   = data.current_page
        state.first_page_url = data.first_page_url
        state.from           = data.from
        state.last_page      = data.last_page
        state.last_page_url  = data.last_page_url
        state.links          = data.links
        state.next_page_url  = data.next_page_url
        state.path           = data.path
        state.per_page       = data.per_page
        state.prev_page_url  = data.prev_page_url
        state.to             = data.to
        state.total          = data.total
    },
    setNewAuthorID(state, id) {
        state.new_author_id = id
    },
    setAuthorData(state, data) {
        state.author_data = data;
    },
    setAsset(state, data) {
        if (!data) {
            state.author_asset = [];
            return
        }
        let {comic} = data

        state.author_asset = comic
    },
    setSearchAsset(state, json) {
        let {data} = json

        state.search_asset = data;
    },
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
