import axios from "axios";
import setMessage from "../../methods/setMessage";

const state     = {
    spider_id   : null,
    cover       : null,
    title       : null,
    title_en    : null,
    gdtm_list   : null,
    current_page: null,
    page_number : null,
    page_list   : null,
}
const getters   = {
    spider_id   : state => state.spider_id,
    cover       : state => state.cover,
    title       : state => state.title,
    title_en    : state => state.title_en,
    gdtm_list   : state => state.gdtm_list,
    current_page: state => state.current_page,
    page_number : state => state.page_number,
    page_list   : state => state.page_list,
}
const actions   = {
    //取得爬蟲對象的初始資料
    getInfo({state, commit, dispatch}, target) {
        dispatch('load/setLoading', {target: 'spider_get_ex_hentai_info', flag: true}, {root: true})

        let url = '/api/spider/exhentai?url=' + target
        axios.get(url).then((res) => {
            commit('setInfo', res.data)
        }).catch(() => {
            setMessage('抓取失敗', 'error')
        }).finally(() => {
            dispatch('load/setLoading', {target: 'spider_get_ex_hentai_info', flag: false}, {root: true})
        })
    },
    clearInfo({state, commit, dispatch}) {
        commit('setInfo', {})
    },
}
const mutations = {
    setInfo(state, data) {
        state.spider_id    = data.spider_id
        state.cover        = data.cover
        state.title        = data.title
        state.title_en     = data.title_en
        state.gdtm_list    = data.gdtm_list
        state.current_page = parseInt(data.current_page)
        state.page_number  = data.page_number
        state.page_list    = data.page_list
    },
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
