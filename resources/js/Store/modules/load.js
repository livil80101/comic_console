let state     = {
    loading: false,

    test: 12,

    //漫畫
    comic_editing             : false,
    comic_author_search       : false,
    comic_upload_page         : false,
    comic_delete_multiple_page: false,
    comic_set_page_sort       : false,

    //爬蟲
    spider_get_ex_hentai_info: false,

    //作者
    author_list         : false,
    author_create       : false,
    author_detail       : false,
    author_detail_edit  : false,
    author_asset        : false,
    author_asset_search : false,
    author_asset_setting: false,
    author_delete       : false,

    //會員
}
let getters   = {
    //注意這個是回傳function
    getLoading(state) {
        return (target) => {
            return state[target]
        }
    },
}
let actions   = {
    setLoading({state, commit}, data) {
        commit('setLoading', data);
    },
    reset({state, commit}) {
        commit('reset');
    }
}
let mutations = {
    /**
     * 重置全部load
     *
     * @param state
     * @param target 指定的 load 形態為 string string[] 兩種
     * @param flag boolean
     * */
    setLoading(state, {target, flag}) {
        let is_array = Array.isArray(target)

        if (is_array) {
            for (let i in target) {
                let name = target[i]

                state[name] = flag
            }
        } else {
            if (target)
                state[target] = flag
            else
                state.loading = flag
        }

    },
    //重置全部
    reset(state) {
        let keys = Object.keys(state);

        for (let i in keys) {
            let name = keys[i]

            state[name] = false
        }
    }
}
export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
