import axios from "axios";
import setMessage from "../../methods/setMessage";
import goTop from "../../methods/goTop";
import {random} from "lodash";

const state     = {
    list         : [],
    current_page : 1, // 當前頁數
    first_page_ur: null,
    from         : null,
    last_page    : null, // 最後一頁
    last_page_url: null,
    links        : null,
    next_page_url: null,
    path         : null,
    per_page     : null,
    prev_page_url: null,
    to           : null,
    total        : null, // 總比數

    loading           : false,
    loading_comic_data: false,
    loading_chapter   : false,
    loading_page      : false,

    count: 15, // 筆數

    // 單筆漫畫資料
    comic_data: null,
    chapters  : [],

    //漫畫資料選擇的功能
    detail_select: '',

    // 章節底下頁
    chapter_data         : null,
    pages                : [],
    page_title           : '',
    page_component       : "",
    page_sort_preview_url: null,

    //編輯漫畫
    editor_search_authors: [],
}
const getters   = {
    list          : state => state.list,
    chapters      : state => state.chapters,
    chapter_data  : state => state.chapter_data,
    pages         : state => state.pages,
    page_title    : state => state.page_title,
    page_component: state => state.page_component,

    page_sort_preview_url: state => state.page_sort_preview_url,

    info(state) {
        return {
            current_page: state.current_page,
            last_page   : state.last_page,
            total       : state.total,
            to          : state.to,
            count       : state.count,
        }
    },
    // 注意這個是漫畫列表的
    page(state) {
        return state.current_page
    },
    links(state) {
        return state.links
    },
    loading           : state => state.loading,
    loading_comic_data: state => state.loading_comic_data,
    loading_chapter   : state => state.loading_chapter,
    loading_page      : state => state.loading_page,
    comic_data        : state => state.comic_data,

    detail_select: state => state.detail_select,

    editor_search_authors: state => state.editor_search_authors,
}
const actions   = {
    //關閉漫畫資料
    clearComicData({state}) {
        state.comic_data = null
    },
    // 漫畫列表
    getComicList({state, commit}) {
        commit('setLoading', {flag: true})
        // 歸零動作
        commit('setComicData', null)
        commit('setChapterList', [])

        axios.get('/api/comic', {
            params: {
                page : state.current_page,
                count: state.count,
            }
        }).then((resp) => {
            commit('setComicList', resp.data)
        }).finally(() => {
            commit('setLoading', {flag: false})
        })
    },
    // 新增漫畫
    addComic({state, commit}, data) {
        commit('setLoading', {flag: true})
        commit('setComicData', null)
        axios.post('/api/comic', data, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            }
        ).then((resp) => {
            commit('setComicData', resp.data)
            commit('unshiftComicList', resp.data)

        }).catch(() => {
            console.log('這邊添加 添加失敗的表現')
        }).finally(() => {
            commit('setLoading', {flag: false})
        })
    },
    // 取得漫畫單筆資料
    getComicDataById({state, commit}, id) {
        commit('setLoading', {flag: true, target: 'comic'})
        axios.get('/api/comic/' + id).then(resp => {
            commit('setComicData', resp.data)
        }).catch(({response}) => {
            commit('setComicData', null)
        }).finally(() => {
            commit('setLoading', {flag: false, target: 'comic'})
        })
    },
    getChapterById({state, commit}, id) {
        commit('setLoading', {flag: true, target: 'chapter'})
        commit('setChapterList', [])
        axios.get('/api/chapters/' + id).then(resp => {
            commit('setChapterList', resp.data)
        }).catch(({response}) => {
            commit('setChapterList', [])
        }).finally(() => {
            commit('setLoading', {flag: false, target: 'chapter'})
        })
    },
    // 新增章節
    addChapter({state, commit}, data) {
        commit('setLoading', {flag: true, target: 'chapter_add'})
        axios.post('/api/add_chapter', data, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            }
        ).then((resp) => {

            commit('setChapterList', resp.data)
            //播放訊息添加成功
            setMessage('添加成功', 'success')
        }).catch(() => {
            setMessage('添加失敗！', 'error')
        }).finally(() => {
            commit('setLoading', {flag: false, target: 'chapter_add'})
        })
    },
    //上傳頁
    addPage({state, commit, dispatch}, data) {
        dispatch('load/setLoading', {target: 'comic_upload_page', flag: true}, {root: true})

        axios.post('/api/add_page', data, {
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        }).then(({data}) => {
            //播放訊息添加成功
            commit('setPageList', {pages: data.pages})
            setMessage('添加成功', 'success')
        }).catch(() => {
            setMessage('添加失敗！', 'error')
        }).finally(() => {
            dispatch('load/setLoading', {target: 'comic_upload_page', flag: false}, {root: true})
        })
    },
    //搜尋作者
    searchAuthor({state, commit, dispatch}, params) {
        dispatch('load/setLoading', {target: 'comic_author_search', flag: true}, {root: true})
        commit('setEditorSearchAuthor', [])

        axios.get('/api/author', {
            params
        }).then(({data}) => {
            commit('setEditorSearchAuthor', data)
        }).finally(() => {
            dispatch('load/setLoading', {target: 'comic_author_search', flag: false}, {root: true})
        })
    },
    //清空搜尋作者
    clearSearchAuthor({commit}) {
        commit('setEditorSearchAuthor', [])
    },
    //編輯漫畫資料
    editComicData({state, commit, dispatch}, params) {
        dispatch('load/setLoading', {target: 'comic_editing', flag: true}, {root: true})

        axios.post('/api/edit_comic', params, {
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        }).then(({data}) => {
            commit('setComicData', data)

            setMessage('修改成功！')
        }).finally(() => {
            dispatch('load/setLoading', {target: 'comic_editing', flag: false}, {root: true})
        })

    },
    getPagesById({state, commit}, id) {
        commit('setLoading', {flag: true, target: 'page'})
        commit('setPageList', [])
        axios.get('/api/chapters/' + id + '/pages').then(resp => {
            commit('setPageList', resp.data)
        }).catch(({response}) => {
            commit('setPageList', [])
        }).finally(() => {
            commit('setLoading', {flag: false, target: 'page'})
        })
    },
    //設定頁面列表
    setPage({commit}, current_page) {
        commit('setPage', current_page)
    },
    //設定頁面標題
    setPageTitle({commit}, title) {
        commit('setPageTitle', title)
    },
    //切換頁面功能
    setPageComponent({commit, dispatch}, component) {
        commit('setPageComponent', component)
    },
    setCount({commit}, count) {
        commit('setCount', count)
    },
    setDetailSelect({commit}, target = 'info') {
        commit('setDetailSelect', target)
        goTop();
    },
    //刪除多頁
    deletePages({state, commit, dispatch}, id) {
        dispatch('load/setLoading', {target: 'comic_delete_multiple_page', flag: true}, {root: true})

        axios.post('/api/delete_page', {
            id
        }).then(({data}) => {
            setMessage('刪除成功')
            commit('setPageList', data)
        }).catch(() => {
            setMessage('刪除成功', 'error')
        }).finally(() => {
            dispatch('load/setLoading', {target: 'comic_delete_multiple_page', flag: false}, {root: true})
        })
    },
    //設定拖曳頁面
    setPageSortPreview({state}, url) {
        state.page_sort_preview_url = url
    },
    setPageSort({state, commit, dispatch}, data) {
        dispatch('load/setLoading', {target: 'comic_set_page_sort', flag: true}, {root: true})

        axios.post('/api/set_page_sort', data).then((resp) => {
            commit('setPageList', resp.data)
            setMessage('修改成功')
        }).catch(() => {
            setMessage('錯誤！', 'error')
        }).finally(() => {
            dispatch('load/setLoading', {target: 'comic_set_page_sort', flag: false}, {root: true})
        })
    },
}
const mutations = {
    setLoading(state, {target, flag}) {
        switch (target) {
            case 'comic':
                state.loading_comic_data = flag
                break
            case 'chapter':
                state.loading_chapter = flag
                break
            case 'chapter_add':
                state.loading_chapter = flag
                break
            case 'page':
                state.loading_page = flag
                break
            default:
                state.loading = flag
        }
    },
    setComicList(state, data) {
        state.list           = data.data
        state.current_page   = data.current_page
        state.first_page_url = data.first_page_url
        state.from           = data.from
        state.last_page      = data.last_page
        state.last_page_url  = data.last_page_url
        state.links          = data.links
        state.next_page_url  = data.next_page_url
        state.path           = data.path
        state.per_page       = data.per_page
        state.prev_page_url  = data.prev_page_url
        state.to             = data.to
        state.total          = data.total
    },
    setChapterList(state, chapters) {
        state.chapters = chapters
    },
    setPageList(state, {comic, chapter, pages}) {
        if (comic)
            state.comic_data = comic
        if (chapter)
            state.chapter_data = chapter
        if (pages)
            state.pages = pages
    },
    setPageTitle(state, title) {
        state.page_title = title
    },
    setPageComponent(state, component) {
        state.page_component = component
    },
    unshiftComicList(state, data) {
        state.list.unshift(data)
    },
    pushChapterList(state, data) {
        state.chapters.push(data)
    },
    setComicData(state, data) {
        if (data)
            data.cover = data.cover + '?' + random(99999)
        state.comic_data = data
    },
    setPage(state, current_page) {
        state.current_page = current_page
    },
    setCount(state, count) {
        state.count = count
    },
    setDetailSelect(state, target) {
        state.detail_select = target
    },
    setEditorSearchAuthor(state, target) {
        state.editor_search_authors = target.data
    },

}


export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
