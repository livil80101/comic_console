let mixObj = {

    /**
     * 檢查是否是 jpg png
     * */
    isImage(type, allow = null) {
        //只接受 jpg png 兩種
        if (!allow)
            allow = ['image/jpeg', 'image/jpg', 'image/png'];
        return allow.indexOf(type) >= 0
    },

    /**
     * 建立圖片預覽url
     * */
    createPreviewURL(item) {
        return URL.createObjectURL(item)
    },

    /**
     * 註銷圖片預覽url
     * */
    removePreviewURL(url) {
        URL.revokeObjectURL(url)
    },
};


export default mixObj
