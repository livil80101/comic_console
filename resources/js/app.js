require('./bootstrap');
import Vue from "vue";

import VueRouter from "vue-router";
import App from "./views/App";

// mixins
import def from "./mixins/def";

Vue.mixin(def)

// library
import image from "./library/image";
import storage from "./library/storage";

// let mixins = [image];
Vue.prototype.$image   = image;
Vue.prototype.$storage = storage;

//自定義
require('./directives/loader');

Vue.use(VueRouter);
import {sync} from 'vuex-router-sync' //研究這個怎麼用
import store from './Store/store'
import router from "./router/router";

document.addEventListener("DOMContentLoaded", function () {
    new Vue({
        el        : "#app",
        components: {
            App
        },
        store,
        router
    });
});
