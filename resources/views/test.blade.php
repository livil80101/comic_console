<!doctype html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<form method="post" enctype="multipart/form-data">

    <input type="text" name="id" value="1">
    <input type="text" name="title" value="111">
    <input type="text" name="description" value="111">
    <input type="text" name="author_id" value="4">
    <input type="text" name="enable" value="1">
    <input type="file" name="cover"
           {{--           webkitdirectory directory --}}
           multiple
    >

    @csrf
    <input type="submit" value="gogo">
</form>
</body>
</html>
