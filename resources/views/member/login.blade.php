<!doctype html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css"/>
    <link rel="stylesheet" href="{{ asset('/member/login.css') }}">
    <title>會員登入</title>
    <script src="{{ asset('/member/login.js') }}"></script>
</head>
<body>
<div class="container" id="container">
    <div class="form-container sign-up-container">
        <form action="#">
            <h1>建立新的帳號</h1>
            <div class="social-container">
                <a href="#" class="social"><i class="fab fa-facebook-f"></i></a>
                <a href="#" class="social"><i class="fab fa-google-plus-g"></i></a>
                <a href="#" class="social"><i class="fab fa-linkedin-in"></i></a>
            </div>
            <span>或是使用您的E-mail建立</span>
            <input type="text" placeholder="名稱"/>
            <input type="email" placeholder="Email"/>
            <input type="password" placeholder="密碼"/>
            <button>註冊</button>
        </form>
    </div>
    <div class="form-container sign-in-container">
        <form action="" method="post">
            {{ csrf_field() }}
            <h1>登入</h1>
            <div class="social-container">
                <a href="#" class="social"><i class="fab fa-facebook-f"></i></a>
                <a href="#" class="social"><i class="fab fa-google-plus-g"></i></a>
                <a href="#" class="social"><i class="fab fa-linkedin-in"></i></a>
            </div>
            <span>或是使用您的帳號登入</span>
            <input name="email" type="email" placeholder="Email" value="{{ $email }}"/>
            <input name="password" type="password" placeholder="密碼" value="{{ $password }}"/>
            <a href="#">忘記密碼?</a>
            <button>登入</button>

            <input type="hidden" name="redirect_url" value="{{ $redirect_url }}">
        </form>
    </div>
    <div class="overlay-container">
        <div class="overlay">
            <div class="overlay-panel overlay-left">
                <h1>歡迎使用本站!</h1>
                <p>點選「註冊」，即表示您同意接受<a href="#">服務條款</a>而且已閱讀<a href="#">隱私權保護政策</a></p>
                <button class="ghost" id="signIn">已經有帳號</button>
            </div>
            <div class="overlay-panel overlay-right">
                <h1>歡迎回來!</h1>
                <p>很高興能繼續為您服務</p>
                <button class="ghost" id="signUp">第一次來到本站</button>
            </div>
        </div>
    </div>
</div>


</body>
</html>
