const mix   = require('laravel-mix');
let webpack = require('webpack');
const path  = require('path');
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */


mix.webpackConfig({
    output: {
        // publicPath: "public",
        path: path.resolve(__dirname, 'public'),

    },

    plugins: [
        new webpack.ProvidePlugin({
            $              : "jquery",
            jQuery         : "jquery",
            jquery         : "jquery",
            "window.jQuery": "jquery",

            _: "lodash",

            axios: "axios",
            Vue  : "vue",
        })
    ],

    // module: {
    //     rules: [
    //         {
    //             test: /\.(eot|svg|ttf|woff|woff2?)$/,
    //             loader: 'url-loader'
    //         }
    //     ]
    // },
});

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/css/app.scss', 'public/css')
    .extract(['vue']);
