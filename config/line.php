<?php
return [
    'channel_id'     => env('LINE_CHANNEL_ID'),
    'channel_secret' => env('LINE_CHANNEL_SECRET'),

    'login_callback_url' => env('LINE_LOGIN_Callback_URL'),

    'bot_token' => env('LINE_BOT_TOKEN'),

    'message_post_url' => 'https://api.line.me/v2/bot/message/push',

    //測試對象
    'test_target'      => [
        env('LINE_TEST_TARGET'),
        env('LINE_TEST_TARGET2'),
    ],
];
