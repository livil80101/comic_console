<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('app');
});
//針對第三方登入
Route::get('api_login', 'LoginController@apiLogin')->name('api.login.page');
Route::post('api_login', 'LoginController@action');

//暫時的會員服務中心
Route::get('member_service', function () {
    dd('暫時的會員服務中心');
})->name('member.service');

Route::get("/test", "TestController@test");
Route::post("/test", "TestController@post");


Route::namespace('Comic')->group(function () {
    //爬蟲的預覽封面
    Route::prefix('cover')->group(function (){
        Route::get('ex_hentai', 'SpiderController@showExHentaiCover');
    });
    Route::prefix('img')->group(function (){
        Route::get('ex_hentai', 'SpiderController@showExHentaiImage');
    });
    Route::prefix('page')->group(function (){
        Route::get('ex_hentai', 'SpiderController@getPageImage');
    });

});

Route::get('{any}', function () {
    return view('app');
})->where(["any" => ".*"]);
