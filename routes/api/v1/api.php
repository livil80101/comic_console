<?php

use Illuminate\Support\Facades\Route;

Route::namespace('v1')->group(function () {
    Route::get('login', 'LoginController@login');

    Route::middleware('auth:members_api')->group(function () {

        Route::get('user_data', 'MemberController@index');

    });
});
