<?php

use App\Models\Comic;
use App\Models\ComicChapter;
use Illuminate\Foundation\Inspiring;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Storage;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->purpose('Display an inspiring quote');

Artisan::command('weil:test', function () {
    //測試區
    $re = [0, 1];

    for ($i = 0; $i < 100; $i++) {
        $re[] = $re[$i] + $re[$i + 1];
    }

    dd($re);
})->purpose('測試區');
