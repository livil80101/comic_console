<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::namespace('Comic')->group(function () {

    Route::get('comic', 'ComicController@getComic');
    Route::post('comic', 'ComicController@addComic');
    Route::get('comic/{id}', 'ComicController@getComicData');// 漫畫資料
    Route::get('chapters/{id}', 'ComicController@getChapters');// 取得漫畫章節
    Route::get('chapters/{id}/pages', 'ComicController@getPages');// 取得漫畫章節底下頁數
    Route::post('add_chapter', "ComicController@addChapter");// 新增章節
    Route::post('edit_comic', "ComicController@editComic");// 編輯漫畫
    Route::post('edit_chapter', "ComicController@editChapter");// 編輯章節
    Route::post('edit_page', "ComicController@editPage");// 編輯頁
    Route::post('set_page_sort', "ComicController@setPageSort");// 編輯頁順序
    Route::post('delete_page', "ComicController@deletePage");// 刪除頁
    Route::post('add_page', "ComicController@addPage");// 新增頁
    Route::get('comic/{id}/message', 'ComicController@getMessage');// 漫畫留言
    Route::get('chapters/{id}/comment', 'ComicController@getComment');// 取得章節留言
    Route::post('comic/{id}/tags', 'ComicController@setComicTags');// 設定漫畫標籤

    //爬蟲
    Route::get('spider/exhentai', 'SpiderController@exHentai');


    // 作者
    Route::get('author', 'AuthorController@getAuthorList');// 作者列表
    Route::get('author/{id}', 'AuthorController@getAuthor');// 作者資料
    Route::get('author/{id}/asset', 'AuthorController@getAuthorAsset');// 作者作品
    Route::post('author/{id}', 'AuthorController@editAuthor');// 編輯作者資料
    Route::delete('author/{id}', 'AuthorController@removeAuthor');//刪除作者
    Route::post('add_author', 'AuthorController@addAuthor');//新增作者

    //標籤
    Route::get('tags', 'TagController@tags');
    Route::get('tags/{id}', 'TagController@comic');

});

Route::namespace('Member')->group(function () {
    //會員
    Route::get('members', 'MemberController@search');
    Route::get('member/{id}', 'MemberController@getMember');
    Route::get('member/{id}/message', 'MemberController@getMessage');
    Route::get('member/{id}/comment', 'MemberController@getComment');


});
