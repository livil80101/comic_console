<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSpidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('spiders', function (Blueprint $table) {
            $table->id();
            $table->integer("chapter_id")->comment('關聯的chapter')->default(null)->nullable();
            $table->string("url")->index()->comment('抓取對象');
            $table->text("html")->comment('暫存html');
            $table->unsignedSmallInteger("type_id")->comment('爬蟲對象');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('spiders');
    }
}
