<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSpidersCol extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('spiders', function (Blueprint $table) {
            $table->string('url_root', 255)->comment('爬蟲隸屬的url');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('spiders', function (Blueprint $table) {
            $table->dropColumn('url_root');
        });
    }
}
