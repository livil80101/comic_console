<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddR18ToComicTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('comic', function (Blueprint $table) {
            $table->boolean('is_r18')->default(false)->comment('是否是成人漫畫');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('comic', function (Blueprint $table) {
            $table->removeColumn('is_r18');
        });
    }
}
