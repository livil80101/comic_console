<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnToSpiderPreview extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('spider_preview', function (Blueprint $table) {
            $table->string('content_type', 15)->nullable()->comment('檔案類型');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('spider_preview', function (Blueprint $table) {
            $table->dropColumn('content_type');
        });
    }
}
