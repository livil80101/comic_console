<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeeDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_data', function (Blueprint $table) {
            $table->unsignedInteger('id')->comment("跟員工id同樣");
            $table->string("name");//名稱
            $table->boolean("enable")->comment("是否開通")->default(false);

            $table->string("avatar", 400)->comment("頭像")->nullable();

            $table->string("pid", 15)->comment("身分證字號")->nullable()->index();
            $table->string("eid", 15)->comment("員工識別代號")->nullable()->index();
            $table->unsignedTinyInteger('sex')->comment('性別')->nullable();
            $table->timestamp("birthday")->comment("生日")->nullable();//
            $table->unsignedTinyInteger("blood")->comment("血型")->nullable();//
            $table->string('tel', 50)->comment("聯絡電話")->nullable();
            $table->string("address", 200)->comment("地址")->default("");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_data');
    }
}
