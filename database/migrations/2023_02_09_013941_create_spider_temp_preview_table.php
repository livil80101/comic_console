<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSpiderTempPreviewTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('spider_temp_preview', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger("spider_id");
            $table->string("style", 300)->comment("縮圖顯示");
            $table->string("url", 300)->comment("抓取對象");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('spider_temp_preview');
    }
}
