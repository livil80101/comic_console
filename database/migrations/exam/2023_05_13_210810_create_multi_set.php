<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * 範例
 * temporary、EXISTS、batch insert 混合成多筆更新/新增功能
 * 負責接收
 */
class CreateMultiSet extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('exam')->create('multi_set', function (Blueprint $table) {
            $table->id();
            $table->string('name')->comment('使用者名稱');
            $table->string('email')->comment('使用者郵件');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('multi_set');
    }
}
