<?php

namespace Database\Seeders;

use App\Models\Author;
use Illuminate\Database\Seeder;

class ComicSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $author              = new Author();
        $author->id          = 1;
        $author->name        = '佚名';
        $author->description = "未確認作者是誰，如果知道的話請通知小編^^";
        $author->save();
    }
}
