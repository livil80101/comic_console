<?php

namespace Database\Seeders;

use App\Models\Tag;
use App\Repositories\Comic\TagRepository;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class TagSeeder extends Seeder
{
    public function __construct(TagRepository $tagRepository)
    {
        $this->tagRepository = $tagRepository;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->tagRepository->setDefaultTags();
    }

    private $tagRepository;

}
