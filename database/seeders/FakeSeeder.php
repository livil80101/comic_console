<?php

namespace Database\Seeders;

use App\Models\Comic;
use App\Models\ComicChapter;
use App\Models\ComicPage;
use App\Repositories\Fake\ComicRepository as FakeComicRepository;
use App\Repositories\Fake\MemberRepository as FakeMemberRepository;
use Illuminate\Database\Seeder;

class FakeSeeder extends Seeder
{
    public function __construct()
    {

    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    }
}
