<?php

namespace Database\Seeders;

use App\Repositories\Fake\ComicRepository as FakeComicRepository;
use App\Repositories\Fake\MemberRepository as FakeMemberRepository;
use Illuminate\Database\Seeder;

class FakeComicSeeder extends Seeder
{
    public function __construct(FakeComicRepository $fakeComicRepository, FakeMemberRepository $fakeMemberRepository)
    {
        $this->fakeComicRepository  = $fakeComicRepository;
        $this->fakeMemberRepository = $fakeMemberRepository;
    }

    private $fakeComicRepository;
    private $fakeMemberRepository;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->command->info('=======生成假資料=======');

        $this->command->info('建立假作者....');
        $this->fakeComicRepository->createAuthor();
        $this->command->info('成功');
        $this->command->info('建立假漫畫....');
        $this->fakeComicRepository->createDefaultComic();
        $this->command->info('成功');
        $this->command->info('建立假標籤....');
        $this->fakeComicRepository->createComicTag();
        $this->command->info('成功');
        $this->command->info('生成假會員....');
        $this->fakeMemberRepository->createDefaultMember();
        $this->command->info('成功');
        $this->command->info('生成假留言....');
        $this->fakeMemberRepository->createDefaultMessage();
        $this->command->info('成功');
    }
}
