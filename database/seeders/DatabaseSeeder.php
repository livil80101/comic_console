<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        $this->call(ComicSeeder::class);//呼叫
        $this->call(TagSeeder::class);
        $this->call(FakeSeeder::class);//呼叫 factory
        $this->call(FakeComicSeeder::class);//生成假漫畫資料
    }
}
