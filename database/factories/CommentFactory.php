<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class CommentFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'member_id'  => rand(1, 20),
            'chapter_id' => rand(1, 300),
            'content'    => $this->faker->text(50),
        ];
    }
}
