<?php

namespace Database\Factories;

use App\Models\ComicChapter;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Ramsey\Uuid\Uuid;

class ComicChapterFactory extends Factory
{
    protected $model = ComicChapter::class;
    static $order = 1;
    static $comic_ic = 0;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $order = self::$order++;

        return [
            'title'    => '第' . ($order+1) . '話',
            'comic_id' => self::$comic_ic,
            'folder' => 'fake',
            'sort'   => $order,
        ];
    }

    //指定漫畫編號
    public function setComicId($id)
    {
        self::$comic_ic = $id;
        self::$order    = 0;
        return $this->state([]);
    }
}
