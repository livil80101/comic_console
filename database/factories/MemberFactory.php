<?php

namespace Database\Factories;

use App\Models\Member;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Hash;


class MemberFactory extends Factory
{
    protected $model = Member::class;
    static private $account_num = 1;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {

        return [
            'name'     => $this->faker->name,
            'account'  => 'a' . self::$account_num++,
            'email'    => $this->faker->email,
            'password' => Hash::make('1111'),
        ];
    }
}
