<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class MessageFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'member_id' => rand(1, 20),
            'comic_id'  => rand(1, 30),
            'content'   => $this->faker->text,
        ];
    }
}
