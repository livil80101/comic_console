<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class ComicPageFactory extends Factory
{

    static $chapter_id = 0;
    static $order = 0;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            "chapter_id" => self::$chapter_id,
            "path"       => '/images/test_page.jpg',
            "url"        => '/images/test_page.jpg',
            'sort'       => self::$order++
        ];
    }

    //指定漫畫章節編號
    public function setChapterId($id)
    {
        self::$chapter_id = $id;
        self::$order      = 0;
        return $this->state([]);
    }
}
